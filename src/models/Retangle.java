package models;

import interfaces.GeometricObject;

public class Retangle implements GeometricObject {
    
    private double width;
    private double height;
    public Retangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.width*this.height;
    }
    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2*(this.width+this.height);
    }
    @Override
    public String toString() {
        return "Retangle [width=" + width + ", height=" + height + "]";
    }

    
}
