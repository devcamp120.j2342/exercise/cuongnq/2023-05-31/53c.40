import models.Circle;
import models.Retangle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle(5);
        Retangle retangle1 = new Retangle(3, 4);

        System.out.println(circle1.toString());
        System.out.println(retangle1.toString());

        System.out.println(circle1.getArea());
        System.out.println(retangle1.getArea());

        System.out.println(circle1.getPerimeter());
        System.out.println(retangle1.getPerimeter());
    }
}
